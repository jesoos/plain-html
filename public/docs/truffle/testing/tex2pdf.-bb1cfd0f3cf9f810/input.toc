\contentsline {section}{Debugging on IDE}{1}{section*.2}
\contentsline {subsection}{Prerequisites}{1}{section*.3}
\contentsline {subsection}{Table of Contents}{1}{section*.4}
\contentsline {subsection}{Truffle Installation}{2}{section*.5}
\contentsline {subsection}{Creating a MetaCoin project}{2}{section*.6}
\contentsline {subsection}{Creating a package.json file}{2}{section*.7}
\contentsline {subsubsection}{Adding Test Script}{2}{section*.8}
\contentsline {subsection}{Truffle Package Installation}{3}{section*.9}
\contentsline {subsection}{The Whole Shell Script}{3}{section*.10}
\contentsline {subsection}{On IntelliJ(or WebStorm) IDE Ultimate Edition}{3}{section*.11}
\contentsline {subsection}{Using OpenZeppelin}{5}{section*.12}
\contentsline {subsection}{Using MultiSigWallet}{7}{section*.13}
\contentsline {subsection}{On Visual Studio Code}{7}{section*.14}
