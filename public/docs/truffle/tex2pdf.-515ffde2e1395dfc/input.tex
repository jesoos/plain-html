\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Truffle \textbar{} Using Truffle Develop and the Console},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Truffle \textbar{} Using Truffle Develop and the Console}
\date{}

\begin{document}
\maketitle

\hypertarget{using-truffle-develop-and-the-console}{%
\section{Using Truffle Develop and the
Console}\label{using-truffle-develop-and-the-console}}

Sometimes it's nice to work with your contracts interactively for
testing and debugging purposes, or for executing transactions by hand.
Truffle provides you two easy ways to do this via an interactive
console, with your contracts available and ready to use.

\begin{itemize}
\tightlist
\item
  \textbf{Truffle console}: A basic interactive console connecting to
  any Ethereum client
\item
  \textbf{Truffle Develop}: An interactive console that also spawns a
  development blockchain
\end{itemize}

\hypertarget{why-two-different-consoles}{%
\subsection{Why two different
consoles?}\label{why-two-different-consoles}}

Having two different consoles allows you to choose the best tool for
your needs.

Reasons to use \textbf{Truffle console}:

\begin{itemize}
\tightlist
\item
  You have a client you're already using, such as
  \href{/docs/ganache/using}{Ganache} or geth
\item
  You want to migrate to a testnet (or the main Ethereum network)
\item
  You want to use a specific mnemonic or account list
\end{itemize}

Reasons to use \textbf{Truffle Develop}:

\begin{itemize}
\tightlist
\item
  You are testing your project with no intention of immediately
  deploying
\item
  You don't need to work with specific accounts (and you're fine with
  using default development accounts)
\item
  You don't want to install and manage a separate blockchain client
\end{itemize}

\hypertarget{commands}{%
\subsection{Commands}\label{commands}}

All commands require that you be in your project folder. You do not need
to be at the root.

\hypertarget{console}{%
\subsubsection{Console}\label{console}}

To launch the console:

\begin{verbatim}
truffle console
\end{verbatim}

This will look for a network definition called \texttt{development} in
the configuration, and connect to it, if available. You can override
this using the \texttt{-\/-network\ \textless{}name\textgreater{}}
option. See more details in the \href{/docs/advanced/networks}{Networks}
section as well as the \href{/docs/advanced/commands}{command
reference}.

When you load the console, you'll immediately see the following prompt:

\begin{verbatim}
truffle(development)>
\end{verbatim}

This tells you you're running within a Truffle console using the
\texttt{development} network.

\hypertarget{truffle-develop}{%
\subsubsection{Truffle Develop}\label{truffle-develop}}

To launch Truffle Develop:

\begin{verbatim}
truffle develop
\end{verbatim}

This will spawn a development blockchain locally on port \texttt{9545},
regardless of what your \texttt{truffle.js} configuration file calls
for.

When you load Truffle Develop, you will see the following:

\begin{verbatim}
Truffle Develop started at http://localhost:9545/

Accounts:
(0) 0x627306090abab3a6e1400e9345bc60c78a8bef57
(1) 0xf17f52151ebef6c7334fad080c5704d77216b732
(2) 0xc5fdf4076b8f3a5357c5e395ab970b5b54098fef
(3) 0x821aea9a577a9b44299b9c15c88cf3087f3b5544
(4) 0x0d1d4e623d10f9fba5db95830f7d3839406c6af2
(5) 0x2932b7a2355d6fecc4b5c0b6bd44cc31df247a2e
(6) 0x2191ef87e392377ec08e7c08eb105ef5448eced5
(7) 0x0f4f2ac550a1b4e2280d04c21cea7ebd822934b5
(8) 0x6330a553fc93768f612722bb8c2ec78ac90b3bbc
(9) 0x5aeda56215b167893e80b4fe645ba6d5bab767de

Private Keys:
(0) c87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3
(1) ae6ae8e5ccbfb04590405997ee2d52d2b330726137b875053c36d94e974d162f
(2) 0dbbe8e4ae425a6d2687f1a7e3ba17bc98c673636790f1b8ad91193c05875ef1
(3) c88b703fb08cbea894b6aeff5a544fb92e78a18e19814cd85da83b71f772aa6c
(4) 388c684f0ba1ef5017716adb5d21a053ea8e90277d0868337519f97bede61418
(5) 659cbb0e2411a44db63778987b1e22153c086a95eb6b18bdf89de078917abc63
(6) 82d052c865f5763aad42add438569276c00d3d88a2d062d36b2bae914d58b8c8
(7) aa3680d5d48a8283413f7a108367c7299ca73f553735860a87b08f39395618b7
(8) 0f62d96d6675f32685bbdb8ac13cda7c23436f63efbb9d07700d8669ff12b7c4
(9) 8d5366123cb560bb606379f90a0bfd4769eecc0557f1b362dcae9012b548b1e5

Mnemonic: candy maple cake sugar pudding cream honey rich smooth crumble sweet treat
\end{verbatim}

This shows you the addresses, private keys, and mnemonic for this
particular blockchain.

\textbf{Note}: The mnemonic and addresses cannot be changed. If you want
to use a different mnemonic or set of addresses, we recommend using
\href{/docs/ganache/using}{Ganache}.

\textbf{Warning}: Remember to never use any of these addresses or the
mnemonic on the mainnet. This is for development only.

\hypertarget{features}{%
\subsection{Features}\label{features}}

Both Truffle Develop and the console provide most of the features
available in the Truffle command line tool. For instance, you can type
\texttt{migrate\ -\/-reset} within the console, and it will be
interpreted the same as if you ran \texttt{truffle\ migrate\ -\/-reset}
on the command line.

Additionally, both Truffle Develop and the console additionally have the
following features:

\begin{itemize}
\item
  All of your compiled contracts are available and ready for use.
\item
  After each command (such as \texttt{migrate\ -\/-reset}) your
  contracts are reprovisioned so you can start using the newly assigned
  addresses and binaries immediately.
\item
  The \texttt{web3} library is made available and is set to connect to
  your Ethereum client.
\item
  All commands that return a promise will automatically be resolved, and
  the result printed, removing the need to use \texttt{.then()} for
  simple commands. For example, the following command:

\begin{verbatim}
MyContract.at("0xabcd...").getValue.call();
\end{verbatim}

  Will return something like:

\begin{verbatim}
5
\end{verbatim}
\end{itemize}

\hypertarget{commands-available}{%
\subsubsection{Commands available}\label{commands-available}}

\begin{itemize}
\tightlist
\item
  \texttt{build}
\item
  \texttt{compile}
\item
  \texttt{create}
\item
  \texttt{debug}
\item
  \texttt{exec}
\item
  \texttt{install}
\item
  \texttt{migrate}
\item
  \texttt{networks}
\item
  \texttt{opcode}
\item
  \texttt{publish}
\item
  \texttt{test}
\item
  \texttt{version}
\end{itemize}

If a Truffle command is not available, it is because it is not relevant
for an existing project (for example, \texttt{init}) or wouldn't make
sense (for example, \texttt{develop} or \texttt{console}).

See full \href{/docs/advanced/commands}{command reference} for more
information.

\end{document}
