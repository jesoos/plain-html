\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Truffle \textbar{} Compiling Contracts},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Truffle \textbar{} Compiling Contracts}
\date{}

\begin{document}
\maketitle

\hypertarget{compiling-contracts}{%
\section{Compiling contracts}\label{compiling-contracts}}

\hypertarget{location}{%
\subsection{Location}\label{location}}

All of your contracts are located in your project's \texttt{contracts/}
directory. As contracts are written in
\href{https://solidity.readthedocs.io/en/develop/}{Solidity}, all files
containing contracts will have a file extension of \texttt{.sol}.
Associated Solidity
\href{http://solidity.readthedocs.org/en/latest/contracts.html\#libraries}{libraries}
will also have a \texttt{.sol} extension.

With a bare Truffle \href{/docs/truffle/quickstart}{project} (created
through \texttt{truffle\ init}), you're given a single
\texttt{Migrations.sol} file that helps in the deployment process. If
you're using a \href{/boxes}{Truffle Box}, you will have multiple files
here.

\hypertarget{command}{%
\subsection{Command}\label{command}}

To compile a Truffle project, change to the root of the directory where
the project is located and then type the following into a terminal:

\begin{verbatim}
truffle compile
\end{verbatim}

Upon first run, all contracts will be compiled. Upon subsequent runs,
Truffle will compile only the contracts that have been changed since the
last compile. If you'd like to override this behavior, run the above
command with the \texttt{-\/-all} option.

\hypertarget{build-artifacts}{%
\subsection{Build artifacts}\label{build-artifacts}}

Artifacts of your compilation will be placed in the
\texttt{build/contracts/} directory, relative to your project root.
(This directory will be created if it does not exist.)

These artifacts are integral to the inner workings of Truffle, and they
play an important part in the successful deployment of your application.
\textbf{You should not edit these files} as they'll be overwritten by
contract compilation and deployment.

\hypertarget{dependencies}{%
\subsection{Dependencies}\label{dependencies}}

You can declare contract dependencies using Solidity's
\href{http://solidity.readthedocs.org/en/latest/layout-of-source-files.html\#importing-other-source-files}{import}
command. Truffle will compile contracts in the correct order and ensure
all dependencies are sent to the compiler. Dependencies can be specified
in two ways:

\hypertarget{importing-dependencies-via-file-name}{%
\subsubsection{Importing dependencies via file
name}\label{importing-dependencies-via-file-name}}

To import contracts from a separate file, add the following code to your
Solidity source file:

\begin{verbatim}
import "./AnotherContract.sol";
\end{verbatim}

This will make all contracts within \texttt{AnotherContract.sol}
available. Here, \texttt{AnotherContract.sol} is relative to the path of
the current contract being written.

Note that Solidity allows other import syntaxes as well. See the
Solidity
\href{http://solidity.readthedocs.org/en/latest/layout-of-source-files.html\#importing-other-source-files}{import
documentation} for more information.

\hypertarget{importing-contracts-from-an-external-package}{%
\subsubsection{Importing contracts from an external
package}\label{importing-contracts-from-an-external-package}}

Truffle supports dependencies installed via both
\href{/docs/truffle/getting-started/package-management-via-ethpm}{EthPM}
and
\href{/docs/truffle/getting-started/package-management-via-npm}{NPM}. To
import contracts from a dependency, use the following syntax

\begin{verbatim}
import "somepackage/SomeContract.sol";
\end{verbatim}

Here, \texttt{somepackage} represents a package installed via EthPM or
NPM, and \texttt{SomeContract.sol} represents a Solidity source file
provided by that package.

Note that Truffle will search installed packages from EthPM first before
searching for packages installed from NPM, so in the rare case of a
naming conflict the package installed via EthPM will be used.

For more information on how to use Truffle's package management
features, please see the Truffle
\href{/docs/truffle/getting-started/package-management-via-ethpm}{EthPM}
and \href{/docs/truffle/getting-started/package-management-via-npm}{NPM}
documentation.

\end{document}
