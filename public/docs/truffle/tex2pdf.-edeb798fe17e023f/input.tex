\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Truffle \textbar{} Truffle Commands},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Truffle \textbar{} Truffle Commands}
\date{}

\begin{document}
\maketitle

\hypertarget{truffle-commands}{%
\section{Truffle Commands}\label{truffle-commands}}

This section will describe every command available in the Truffle
application.

\hypertarget{usage}{%
\subsection{Usage}\label{usage}}

All commands are in the following form:

\begin{verbatim}
truffle [command] [options]
\end{verbatim}

Passing no arguments is equivalent to \texttt{truffle\ help}, which will
display a list of all commands and then exit.

\hypertarget{available-commands}{%
\subsection{Available commands}\label{available-commands}}

\hypertarget{build}{%
\subsubsection{build}\label{build}}

Execute build pipeline (if configuration present).

\begin{verbatim}
truffle build
\end{verbatim}

Requires the \texttt{build} key to be present in the configuration. See
the \href{/docs/getting_started/build}{Building your application} and
\href{/docs/advanced/build_processes}{Build processes} sections for more
details.

\textbf{Alert}: This command is deprecated. Please use third-party build
tools like webpack or grunt, or see our \href{/boxes}{Truffle Boxes} for
an example.

\hypertarget{compile}{%
\subsubsection{compile}\label{compile}}

Compile contract source files.

\begin{verbatim}
truffle compile [--all] [--network <name>]
\end{verbatim}

This will only compile contracts that have changed since the last
compile, unless otherwise specified.

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-\/-all}: Compile all contracts instead of only the contracts
  changed since last compile.
\item
  \texttt{-\/-network\ \textless{}name\textgreater{}}: Specify the
  network to use, saving artifacts specific to that network. Network
  name must exist in the configuration.
\end{itemize}

\hypertarget{console}{%
\subsubsection{console}\label{console}}

Run a console with contract abstractions and commands available.

\begin{verbatim}
truffle console [--network <name>] [--verbose-rpc]
\end{verbatim}

Spawns an interface to interact with contracts via the command line.
Additionally, many Truffle commands are available within the console
(without the \texttt{truffle} prefix).

Requires an external Ethereum client, such as
\href{/docs/ganache/using}{Ganache} or geth. For a console that creates
a development and test environment, use \texttt{truffle\ develop}.

See the \href{/docs/getting_started/console}{Using the console} section
for more details.

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-\/-network\ \textless{}name\textgreater{}}: Specify the
  network to use. Network name must exist in the configuration.
\item
  \texttt{-\/-verbose-rpc}: Log communication between Truffle and the
  Ethereum client.
\end{itemize}

\hypertarget{create}{%
\subsubsection{create}\label{create}}

Helper to create new contracts, migrations and tests.

\begin{verbatim}
truffle create (contract|migration|test) <ArtifactName>
\end{verbatim}

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{contract}: Create a new contract definition and file
  \texttt{contracts/ArtifactName.sol}.
\item
  \texttt{migration}: Create a new migration and file
  \texttt{migrations/\#\#\#\#\#\#\#\#\#\#\#\_artifact\_name.js}.
\item
  \texttt{test}: Create a new test and file
  \texttt{tests/artifact\_name.js}.
\item
  \texttt{\textless{}ArtifactName\textgreater{}}: Name of new artifact.
\end{itemize}

Camel case names of artifacts will be converted to underscore-separated
file names for the migrations and tests. Number prefixes for migrations
are automatically generated.

\hypertarget{debug}{%
\subsubsection{debug}\label{debug}}

Interactively debug any transaction on the blockchain.

\begin{verbatim}
truffle debug <transaction_hash>
\end{verbatim}

Will start an interactive debugging session on a particular transaction.
Allows you to step through each action and replay. See the
\href{/docs/getting_started/debugging}{Debugging your contracts} section
for more details.

\textbf{Alert}: This command is considered experimental.

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{\textless{}transaction\_hash\textgreater{}}: Transaction ID to
  use for debugging.
\end{itemize}

\hypertarget{deploy}{%
\subsubsection{deploy}\label{deploy}}

Alias for \texttt{migrate}. See \texttt{migrate} for details.

\hypertarget{develop}{%
\subsubsection{develop}\label{develop}}

Open a console with a development blockchain

\begin{verbatim}
truffle develop
\end{verbatim}

Spawns a local development blockchain, and allows you to interact with
contracts via the command line. Additionally, many Truffle commands are
available within the console (without the \texttt{truffle} prefix).

If you want an interactive console but want to use an existing
blockchain, use \texttt{truffle\ console}.

See the \href{/docs/getting_started/console}{Using the console} section
for more details.

\hypertarget{exec}{%
\subsubsection{exec}\label{exec}}

Execute a JS module within the Truffle environment.

\begin{verbatim}
truffle exec <script.js> [--network <name>]
\end{verbatim}

This will include \texttt{web3}, set the default provider based on the
network specified (if any), and include your contracts as global objects
while executing the script. Your script must export a function that
Truffle can run.

See the \href{/docs/getting_started/scripts}{Writing external scripts}
section for more details.

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{\textless{}script.js\textgreater{}}: JavaScript file to be
  executed. Can include path information if the script does not exist in
  the current directory.
\end{itemize}

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-\/-network\ \textless{}name\textgreater{}}: Specify the
  network to use, using artifacts specific to that network. Network name
  must exist in the configuration.
\end{itemize}

\hypertarget{help}{%
\subsubsection{help}\label{help}}

Display a list of all commands and then exit.

\begin{verbatim}
truffle help
\end{verbatim}

\hypertarget{init}{%
\subsubsection{init}\label{init}}

Initialize new and empty Ethereum project

\begin{verbatim}
truffle init
\end{verbatim}

Creates a new and empty Truffle project within the current working
directory. Takes no arguments.

\textbf{Alert}: Older versions of Truffle used
\texttt{truffle\ init\ bare} to create an empty project. This usage has
been deprecated. Those looking for the MetaCoin example that used to be
available through \texttt{truffle\ init} should use
\texttt{truffle\ unbox\ MetaCoin} instead.

\hypertarget{install}{%
\subsubsection{install}\label{install}}

Install a package from the Ethereum Package Registry.

\begin{verbatim}
truffle install [package_name]<@version>
\end{verbatim}

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{package\_name}: Name of the package as listed in the Ethereum
  Package Registry.
\end{itemize}

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{\textless{}@version\textgreater{}}: When specified, will
  install a specific version of the package, otherwise will install the
  latest version.
\end{itemize}

See the \href{/docs/getting_started/packages-ethpm}{Package Management
with EthPM} section for more details.

\hypertarget{migrate}{%
\subsubsection{migrate}\label{migrate}}

Run migrations to deploy contracts.

\begin{verbatim}
truffle migrate [--reset] [-f <number>] [--network <name>] [--compile-all] [--verbose-rpc]
\end{verbatim}

Unless specified, this will run from the last completed migration. See
the \href{/docs/getting_started/migrations}{Migrations} section for more
details.

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-\/-reset}: Run all migrations from the beginning, instead of
  running from the last completed migration.
\item
  \texttt{-f\ \textless{}number\textgreater{}}: Run contracts from a
  specific migration. The number refers to the prefix of the migration
  file.
\item
  \texttt{-\/-network\ \textless{}name\textgreater{}}: Specify the
  network to use, saving artifacts specific to that network. Network
  name must exist in the configuration.
\item
  \texttt{-\/-compile-all}: Compile all contracts instead of
  intelligently choosing which contracts need to be compiled.
\item
  \texttt{-\/-verbose-rpc}: Log communication between Truffle and the
  Ethereum client.
\end{itemize}

\hypertarget{networks}{%
\subsubsection{networks}\label{networks}}

Show addresses for deployed contracts on each network.

\begin{verbatim}
truffle networks [--clean]
\end{verbatim}

Use this command before publishing your package to see if there are any
extraneous network artifacts you don't want published. With no options
specified, this package will simply output the current artifact state.

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-\/-clean}: Remove all network artifacts that aren't
  associated with a named network.
\end{itemize}

\hypertarget{opcode}{%
\subsubsection{opcode}\label{opcode}}

Print the compiled opcodes for a given contract.

\begin{verbatim}
truffle opcode <contract_name>
\end{verbatim}

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{\textless{}contract\_name\textgreater{}}: Name of the contract
  to print opcodes for. Must be a contract name, not a file name.
\end{itemize}

\hypertarget{publish}{%
\subsubsection{publish}\label{publish}}

Publish a package to the Ethereum Package Registry.

\begin{verbatim}
truffle publish
\end{verbatim}

All parameters are pulled from your project's configuration file. Takes
no arguments. See the
\href{/docs/getting_started/packages-ethpm}{Package Management with
EthPM} section for more details.

\hypertarget{serve}{%
\subsubsection{serve}\label{serve}}

Serve the built app from \texttt{http://127.0.0.1:8080}, rebuilding and
redeploying changes as needed. Similar to \texttt{truffle\ watch}, but
with the web server component added.

\begin{verbatim}
truffle serve [-p <port>] [--network <name>]
\end{verbatim}

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-p\ \textless{}port\textgreater{}}: Specify the port to serve
  on. Default is 8080.
\item
  \texttt{-\/-network\ \textless{}name\textgreater{}}: Specify the
  network to use, using artifacts specific to that network. Network name
  must exist in the configuration.
\end{itemize}

\textbf{Alert}: This command is deprecated. Please use third-party
development servers like
\href{https://github.com/webpack/webpack-dev-server}{webpack-dev-server}
instead. See our \href{/boxes/webpack}{Webpack Truffle Box} for an
example.

\hypertarget{test}{%
\subsubsection{test}\label{test}}

Run JavaScript and Solidity tests.

\begin{verbatim}
truffle test <test_file> [--compile-all] [--network <name>] [--verbose-rpc]
\end{verbatim}

Runs some or all tests within the \texttt{test/} directory as specified.
See the section on \href{/docs/getting_started/testing}{Testing your
contracts} for more information.

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{\textless{}test\_file\textgreater{}}: Name of the test file to
  be run. Can include path information if the file does not exist in the
  current directory.
\end{itemize}

Optional parameters:

\begin{itemize}
\tightlist
\item
  \texttt{-\/-compile-all}: Compile all contracts instead of
  intelligently choosing which contracts need to be compiled.
\item
  \texttt{-\/-network\ \textless{}name\textgreater{}}: Specify the
  network to use, using artifacts specific to that network. Network name
  must exist in the configuration.
\item
  \texttt{-\/-verbose-rpc}: Log communication between Truffle and the
  Ethereum client.
\end{itemize}

\hypertarget{unbox}{%
\subsubsection{unbox}\label{unbox}}

Download a Truffle Box, a pre-built Truffle project.

\begin{verbatim}
truffle unbox <box_name>
\end{verbatim}

Downloads a \href{/boxes}{Truffle Box} to the current working directory.
See the \href{/boxes}{list of available boxes}.

Parameters:

\begin{itemize}
\tightlist
\item
  \texttt{\textless{}box\_name\textgreater{}}: Name of the Truffle Box.
\end{itemize}

\hypertarget{version}{%
\subsubsection{version}\label{version}}

Show version number and exit.

\begin{verbatim}
truffle version
\end{verbatim}

\hypertarget{watch}{%
\subsubsection{watch}\label{watch}}

Watch filesystem for changes and rebuild the project automatically.

\begin{verbatim}
truffle watch
\end{verbatim}

This command will initiate a watch for changes to contracts,
application, and configuration files. When there's a change, it will
rebuild the app as necessary. Similar to \texttt{truffle\ serve}, but
without the web server component.

\textbf{Alert}: This command is deprecated. Please use external tools to
watch for filesystem changes and rerun tests.

\end{document}
