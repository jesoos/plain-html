\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Truffle \textbar{} Writing Tests in JavaScript},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{color}
\usepackage{fancyvrb}
\newcommand{\VerbBar}{|}
\newcommand{\VERB}{\Verb[commandchars=\\\{\}]}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
% Add ',fontsize=\small' for more characters per line
\newenvironment{Shaded}{}{}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{#1}}}
\newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.49,0.56,0.16}{#1}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{#1}}
\newcommand{\BuiltInTok}[1]{#1}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textit{#1}}}
\newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.53,0.00,0.00}{#1}}
\newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{#1}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.56,0.13,0.00}{#1}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{#1}}
\newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.73,0.13,0.13}{\textit{#1}}}
\newcommand{\ErrorTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{#1}}}
\newcommand{\ExtensionTok}[1]{#1}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{#1}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.02,0.16,0.49}{#1}}
\newcommand{\ImportTok}[1]{#1}
\newcommand{\InformationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{#1}}}
\newcommand{\NormalTok}[1]{#1}
\newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.40,0.40,0.40}{#1}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{#1}}
\newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.74,0.48,0.00}{#1}}
\newcommand{\RegionMarkerTok}[1]{#1}
\newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.73,0.40,0.53}{#1}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\VariableTok}[1]{\textcolor[rgb]{0.10,0.09,0.49}{#1}}
\newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\WarningTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Truffle \textbar{} Writing Tests in JavaScript}
\date{}

\begin{document}
\maketitle

\hypertarget{writing-tests-in-javascript}{%
\section{Writing Tests in
JavaScript}\label{writing-tests-in-javascript}}

Truffle uses the \href{https://mochajs.org/}{Mocha} testing framework
and \href{http://chaijs.com/}{Chai} for assertions to provide you with a
solid framework from which to write your JavaScript tests. Let's dive in
and see how Truffle builds on top of Mocha to make testing your
contracts a breeze.

Note: If you're unfamiliar with writing unit tests in Mocha, please see
\href{https://mochajs.org/}{Mocha's documentation} before continuing.

\hypertarget{use-contract-instead-of-describe}{%
\subsection{Use contract() instead of
describe()}\label{use-contract-instead-of-describe}}

Structurally, your tests should remain largely unchanged from that of
Mocha: Your tests should exist in the \texttt{./test} directory, they
should end with a \texttt{.js} extension, and they should contain code
that Mocha will recognize as an automated test. What makes Truffle tests
different from that of Mocha is the \texttt{contract()} function: This
function works exactly like \texttt{describe()} except it enables
Truffle's
\href{/docs/truffle/testing/testing-your-contracts\#clean-room-environment}{clean-room
features}. It works like this:

\begin{itemize}
\tightlist
\item
  Before each \texttt{contract()} function is run, your contracts are
  redeployed to the running Ethereum client so the tests within it run
  with a clean contract state.
\item
  The \texttt{contract()} function provides a list of accounts made
  available by your Ethereum client which you can use to write tests.
\end{itemize}

Since Truffle uses Mocha under the hood, you can still use
\texttt{describe()} to run normal Mocha tests whenever Truffle
clean-room features are unnecessary.

\hypertarget{use-contract-abstractions-within-your-tests}{%
\subsection{Use contract abstractions within your
tests}\label{use-contract-abstractions-within-your-tests}}

Contract abstractions are the basis for making contract interaction
possible from JavaScript (they're basically our
\href{https://www.youtube.com/watch?v=EhU862ONFys}{flux capacitor}).
Because Truffle has no way of detecting which contracts you'll need to
interact with within your tests, you'll need to ask for those contracts
explicitly. You do this by using the \texttt{artifacts.require()}
method, a method provided by Truffle that allows you to request a usable
contract abstraction for a specific Solidity contract. As you'll see in
the example below, you can then use this abstraction to make sure your
contracts are working properly.

For more information on using contract abstractions, see the
\href{/docs/truffle/getting-started/interacting-with-your-contracts}{Interacting
With Your Contracts} section.

\hypertarget{using-artifacts.require}{%
\subsection{Using artifacts.require()}\label{using-artifacts.require}}

Using \texttt{artifacts.require()} within your tests works the same way
as using it within your migrations; you just need to pass the name of
the contract. See the
\href{/docs/truffle/getting-started/running-migrations\#artifacts-require-}{artifacts.require()
documentation} in the Migrations section for detailed usage.

\hypertarget{using-web3}{%
\subsection{Using web3}\label{using-web3}}

A \texttt{web3} instance is available in each test file, configured to
the correct provider. So calling \texttt{web3.eth.getBalance} just
works!

\hypertarget{examples}{%
\subsection{Examples}\label{examples}}

\hypertarget{using-.then}{%
\subsubsection{\texorpdfstring{Using
\texttt{.then}}{Using .then}}\label{using-.then}}

Here's an example test provided in the \href{/boxes/metacoin}{MetaCoin
Truffle Box}. Note the use of the \texttt{contract()} function, the
\texttt{accounts} array for specifying available Ethereum accounts, and
our use of \texttt{artifacts.require()} for interacting directly with
our contracts.

File: \texttt{./test/metacoin.js}

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{var}\NormalTok{ MetaCoin }\OperatorTok{=} \VariableTok{artifacts}\NormalTok{.}\AttributeTok{require}\NormalTok{(}\StringTok{"MetaCoin"}\NormalTok{)}\OperatorTok{;}

\AttributeTok{contract}\NormalTok{(}\StringTok{'MetaCoin'}\OperatorTok{,} \KeywordTok{function}\NormalTok{(accounts) }\OperatorTok{\{}
  \AttributeTok{it}\NormalTok{(}\StringTok{"should put 10000 MetaCoin in the first account"}\OperatorTok{,} \KeywordTok{function}\NormalTok{() }\OperatorTok{\{}
    \ControlFlowTok{return} \VariableTok{MetaCoin}\NormalTok{.}\AttributeTok{deployed}\NormalTok{().}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(instance) }\OperatorTok{\{}
      \ControlFlowTok{return} \VariableTok{instance}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(accounts[}\DecValTok{0}\NormalTok{])}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(balance) }\OperatorTok{\{}
      \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(}\VariableTok{balance}\NormalTok{.}\AttributeTok{valueOf}\NormalTok{()}\OperatorTok{,} \DecValTok{10000}\OperatorTok{,} \StringTok{"10000 wasn't in the first account"}\NormalTok{)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
  \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
  \AttributeTok{it}\NormalTok{(}\StringTok{"should call a function that depends on a linked library"}\OperatorTok{,} \KeywordTok{function}\NormalTok{() }\OperatorTok{\{}
    \KeywordTok{var}\NormalTok{ meta}\OperatorTok{;}
    \KeywordTok{var}\NormalTok{ metaCoinBalance}\OperatorTok{;}
    \KeywordTok{var}\NormalTok{ metaCoinEthBalance}\OperatorTok{;}

    \ControlFlowTok{return} \VariableTok{MetaCoin}\NormalTok{.}\AttributeTok{deployed}\NormalTok{().}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(instance) }\OperatorTok{\{}
\NormalTok{      meta }\OperatorTok{=}\NormalTok{ instance}\OperatorTok{;}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(accounts[}\DecValTok{0}\NormalTok{])}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(outCoinBalance) }\OperatorTok{\{}
\NormalTok{      metaCoinBalance }\OperatorTok{=} \VariableTok{outCoinBalance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\VariableTok{getBalanceInEth}\NormalTok{.}\AttributeTok{call}\NormalTok{(accounts[}\DecValTok{0}\NormalTok{])}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(outCoinBalanceEth) }\OperatorTok{\{}
\NormalTok{      metaCoinEthBalance }\OperatorTok{=} \VariableTok{outCoinBalanceEth}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{() }\OperatorTok{\{}
      \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(metaCoinEthBalance}\OperatorTok{,} \DecValTok{2} \OperatorTok{*}\NormalTok{ metaCoinBalance}\OperatorTok{,} \StringTok{"Library function returned unexpected function, linkage may be broken"}\NormalTok{)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
  \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
  \AttributeTok{it}\NormalTok{(}\StringTok{"should send coin correctly"}\OperatorTok{,} \KeywordTok{function}\NormalTok{() }\OperatorTok{\{}
    \KeywordTok{var}\NormalTok{ meta}\OperatorTok{;}

    \CommentTok{// Get initial balances of first and second account.}
    \KeywordTok{var}\NormalTok{ account_one }\OperatorTok{=}\NormalTok{ accounts[}\DecValTok{0}\NormalTok{]}\OperatorTok{;}
    \KeywordTok{var}\NormalTok{ account_two }\OperatorTok{=}\NormalTok{ accounts[}\DecValTok{1}\NormalTok{]}\OperatorTok{;}

    \KeywordTok{var}\NormalTok{ account_one_starting_balance}\OperatorTok{;}
    \KeywordTok{var}\NormalTok{ account_two_starting_balance}\OperatorTok{;}
    \KeywordTok{var}\NormalTok{ account_one_ending_balance}\OperatorTok{;}
    \KeywordTok{var}\NormalTok{ account_two_ending_balance}\OperatorTok{;}

    \KeywordTok{var}\NormalTok{ amount }\OperatorTok{=} \DecValTok{10}\OperatorTok{;}

    \ControlFlowTok{return} \VariableTok{MetaCoin}\NormalTok{.}\AttributeTok{deployed}\NormalTok{().}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(instance) }\OperatorTok{\{}
\NormalTok{      meta }\OperatorTok{=}\NormalTok{ instance}\OperatorTok{;}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_one)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(balance) }\OperatorTok{\{}
\NormalTok{      account_one_starting_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_two)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(balance) }\OperatorTok{\{}
\NormalTok{      account_two_starting_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\AttributeTok{sendCoin}\NormalTok{(account_two}\OperatorTok{,}\NormalTok{ amount}\OperatorTok{,} \OperatorTok{\{}\DataTypeTok{from}\OperatorTok{:}\NormalTok{ account_one}\OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{() }\OperatorTok{\{}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_one)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(balance) }\OperatorTok{\{}
\NormalTok{      account_one_ending_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
      \ControlFlowTok{return} \VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_two)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{).}\AttributeTok{then}\NormalTok{(}\KeywordTok{function}\NormalTok{(balance) }\OperatorTok{\{}
\NormalTok{      account_two_ending_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}

      \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(account_one_ending_balance}\OperatorTok{,}\NormalTok{ account_one_starting_balance }\OperatorTok{-}\NormalTok{ amount}\OperatorTok{,} \StringTok{"Amount wasn't correctly taken from the sender"}\NormalTok{)}\OperatorTok{;}
      \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(account_two_ending_balance}\OperatorTok{,}\NormalTok{ account_two_starting_balance }\OperatorTok{+}\NormalTok{ amount}\OperatorTok{,} \StringTok{"Amount wasn't correctly sent to the receiver"}\NormalTok{)}\OperatorTok{;}
    \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
  \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
\OperatorTok{\}}\NormalTok{)}\OperatorTok{;}
\end{Highlighting}
\end{Shaded}

This test will produce the following output:

\begin{verbatim}
  Contract: MetaCoin
    √ should put 10000 MetaCoin in the first account (83ms)
    √ should call a function that depends on a linked library (43ms)
    √ should send coin correctly (122ms)


  3 passing (293ms)
\end{verbatim}

\hypertarget{using-asyncawait}{%
\subsubsection{Using async/await}\label{using-asyncawait}}

Here is a similar example, but using
\href{https://javascript.info/async-await}{async/await} notation:

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{const}\NormalTok{ MetaCoin }\OperatorTok{=} \VariableTok{artifacts}\NormalTok{.}\AttributeTok{require}\NormalTok{(}\StringTok{"MetaCoin"}\NormalTok{)}\OperatorTok{;}

\AttributeTok{contract}\NormalTok{(}\StringTok{'2nd MetaCoin test'}\OperatorTok{,} \AttributeTok{async}\NormalTok{ (accounts) }\OperatorTok{=>} \OperatorTok{\{}

  \AttributeTok{it}\NormalTok{(}\StringTok{"should put 10000 MetaCoin in the first account"}\OperatorTok{,} \AttributeTok{async}\NormalTok{ () }\OperatorTok{=>} \OperatorTok{\{}
     \KeywordTok{let}\NormalTok{ instance }\OperatorTok{=}\NormalTok{ await }\VariableTok{MetaCoin}\NormalTok{.}\AttributeTok{deployed}\NormalTok{()}\OperatorTok{;}
     \KeywordTok{let}\NormalTok{ balance }\OperatorTok{=}\NormalTok{ await }\VariableTok{instance}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(accounts[}\DecValTok{0}\NormalTok{])}\OperatorTok{;}
     \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(}\VariableTok{balance}\NormalTok{.}\AttributeTok{valueOf}\NormalTok{()}\OperatorTok{,} \DecValTok{10000}\NormalTok{)}\OperatorTok{;}
  \OperatorTok{\}}\NormalTok{)}
  
  \AttributeTok{it}\NormalTok{(}\StringTok{"should call a function that depends on a linked library"}\OperatorTok{,} \AttributeTok{async}\NormalTok{ () }\OperatorTok{=>} \OperatorTok{\{}
    \KeywordTok{let}\NormalTok{ meta }\OperatorTok{=}\NormalTok{ await }\VariableTok{MetaCoin}\NormalTok{.}\AttributeTok{deployed}\NormalTok{()}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ outCoinBalance }\OperatorTok{=}\NormalTok{ await }\VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(accounts[}\DecValTok{0}\NormalTok{])}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ metaCoinBalance }\OperatorTok{=} \VariableTok{outCoinBalance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ outCoinBalanceEth }\OperatorTok{=}\NormalTok{ await }\VariableTok{meta}\NormalTok{.}\VariableTok{getBalanceInEth}\NormalTok{.}\AttributeTok{call}\NormalTok{(accounts[}\DecValTok{0}\NormalTok{])}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ metaCoinEthBalance }\OperatorTok{=} \VariableTok{outCoinBalanceEth}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
    \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(metaCoinEthBalance}\OperatorTok{,} \DecValTok{2} \OperatorTok{*}\NormalTok{ metaCoinBalance)}\OperatorTok{;}

  \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}

  \AttributeTok{it}\NormalTok{(}\StringTok{"should send coin correctly"}\OperatorTok{,} \AttributeTok{async}\NormalTok{ () }\OperatorTok{=>} \OperatorTok{\{}

    \CommentTok{// Get initial balances of first and second account.}
    \KeywordTok{let}\NormalTok{ account_one }\OperatorTok{=}\NormalTok{ accounts[}\DecValTok{0}\NormalTok{]}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ account_two }\OperatorTok{=}\NormalTok{ accounts[}\DecValTok{1}\NormalTok{]}\OperatorTok{;}

    \KeywordTok{let}\NormalTok{ amount }\OperatorTok{=} \DecValTok{10}\OperatorTok{;}

    
    \KeywordTok{let}\NormalTok{ instance }\OperatorTok{=}\NormalTok{ await }\VariableTok{MetaCoin}\NormalTok{.}\AttributeTok{deployed}\NormalTok{()}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ meta }\OperatorTok{=}\NormalTok{ instance}\OperatorTok{;}

    \KeywordTok{let}\NormalTok{ balance }\OperatorTok{=}\NormalTok{ await }\VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_one)}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ account_one_starting_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}

\NormalTok{    balance }\OperatorTok{=}\NormalTok{ await }\VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_two)}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ account_two_starting_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
\NormalTok{    await }\VariableTok{meta}\NormalTok{.}\AttributeTok{sendCoin}\NormalTok{(account_two}\OperatorTok{,}\NormalTok{ amount}\OperatorTok{,} \OperatorTok{\{}\DataTypeTok{from}\OperatorTok{:}\NormalTok{ account_one}\OperatorTok{\}}\NormalTok{)}\OperatorTok{;}

\NormalTok{    balance }\OperatorTok{=}\NormalTok{ await }\VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_one)}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ account_one_ending_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}

\NormalTok{    balance }\OperatorTok{=}\NormalTok{ await }\VariableTok{meta}\NormalTok{.}\VariableTok{getBalance}\NormalTok{.}\AttributeTok{call}\NormalTok{(account_two)}\OperatorTok{;}
    \KeywordTok{let}\NormalTok{ account_two_ending_balance }\OperatorTok{=} \VariableTok{balance}\NormalTok{.}\AttributeTok{toNumber}\NormalTok{()}\OperatorTok{;}
    
    \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(account_one_ending_balance}\OperatorTok{,}\NormalTok{ account_one_starting_balance }\OperatorTok{-}\NormalTok{ amount}\OperatorTok{,} \StringTok{"Amount wasn't correctly taken from the sender"}\NormalTok{)}\OperatorTok{;}
    \VariableTok{assert}\NormalTok{.}\AttributeTok{equal}\NormalTok{(account_two_ending_balance}\OperatorTok{,}\NormalTok{ account_two_starting_balance }\OperatorTok{+}\NormalTok{ amount}\OperatorTok{,} \StringTok{"Amount wasn't correctly sent to the receiver"}\NormalTok{)}\OperatorTok{;}
  \OperatorTok{\}}\NormalTok{)}\OperatorTok{;}

\OperatorTok{\}}\NormalTok{)}
\end{Highlighting}
\end{Shaded}

This test will produce identical output to the previous example.

\hypertarget{specifying-tests}{%
\subsection{Specifying tests}\label{specifying-tests}}

You can limit the tests being executed to a specific file as follows:

\begin{verbatim}
truffle test ./test/metacoin.js
\end{verbatim}

See the full
\href{/docs/truffle/reference/truffle-commands\#test}{command reference}
for more information.

\hypertarget{advanced}{%
\subsection{Advanced}\label{advanced}}

Truffle gives you access to Mocha's configuration so you can change how
Mocha behaves. See the
\href{/docs/advanced/configuration\#mocha}{project configuration}
section for more details.

\end{document}
