\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Truffle \textbar{} Choosing an Ethereum Client},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Truffle \textbar{} Choosing an Ethereum Client}
\date{}

\begin{document}
\maketitle

\hypertarget{choosing-an-ethereum-client}{%
\section{Choosing an Ethereum
client}\label{choosing-an-ethereum-client}}

There are many Ethereum clients to choose from. We recommend different
clients depending on whether you are developing or deploying.

\hypertarget{when-developing}{%
\subsection{When developing}\label{when-developing}}

\hypertarget{ganache}{%
\subsubsection{Ganache}\label{ganache}}

We recommend \href{/ganache}{Ganache}, a personal blockchain for
Ethereum development that runs on your desktop. Part of the Truffle
Suite, Ganache simplifies dapp development by placing your contracts and
transactions front and center. Using Ganache you can quickly see how
your application affects the blockchain, and introspect details like
your accounts, balances, contract creations and gas costs. You can also
fine tune Ganache's advanced mining controls to better suit your needs.
Ganache is available for Windows, Mac and Linux, and you can
\href{/ganache}{download it here}.

Ganache, when launched, runs on \texttt{http://127.0.0.1:7545}. It will
display the first 10 accounts and the mnemonic used to create those
accounts.
(\href{https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki}{Read
more about account mnemonics}.) The mnemonic will persist across
restarts of Ganache, though it can be changed to be randomly generated.
You can also \href{/docs/ganache/using}{input your own}.

\textbf{Warning}: Do not use this mnemonic on the main Ethereum network
(mainnet).

\hypertarget{truffle-develop}{%
\subsubsection{Truffle Develop}\label{truffle-develop}}

We also recommend using Truffle Develop, a development blockchain built
directly into Truffle. Truffle Develop helps you set up an integrated
blockchain environment with a single command, no installation required.
Run Truffle Develop by typing the following into a terminal:

\begin{verbatim}
truffle develop
\end{verbatim}

This will run the client on \texttt{http://127.0.0.1:9545}. It will
display the first 10 accounts and the mnemonic used to create those
accounts.
(\href{https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki}{Read
more about account mnemonics}.) Truffle Develop uses the same mnemonic
every time:

\begin{verbatim}
candy maple cake sugar pudding cream honey rich smooth crumble sweet treat
\end{verbatim}

\textbf{Warning}: Do not use this mnemonic on the main Ethereum network
(mainnet). If you send ETH to any account generated from this mnemonic,
you will lose it all!

Once launched, Truffle Develop will provide you with a console you can
use to run all available Truffle commands. These commands are input by
omitting the \texttt{truffle} prefix. So, for example, to compile your
smart contracts, instead of typing \texttt{truffle\ compile}, you need
to only type \texttt{compile}.

To read more about interacting with the console, please see the
\href{/docs/truffle/getting-started/using-truffle-develop-and-the-console}{Using
the Console} section.

\hypertarget{ganache-cli}{%
\subsubsection{Ganache CLI}\label{ganache-cli}}

Ganache also has a command-line interface for those who aren't working
from a graphical environment. Great for automated testing and continuous
integration environments, Ganache CLI runs headless and can be
configured to serve all your development needs. Ganache CLI processes
transactions instantly instead of waiting for the default block time, so
you can test that your code works quickly. It also tells you immediately
when your smart contracts run into errors, and integrates directly with
Truffle to reduce test runtime up to 90\% compared to other clients.
\href{https://github.com/trufflesuite/ganache-cli/}{Learn more about
Ganache CLI}.

\hypertarget{deploying-to-live-networks}{%
\subsection{Deploying to live
networks}\label{deploying-to-live-networks}}

There are many official and unofficial Ethereum clients available for
you to use. The following is a short list:

\begin{itemize}
\tightlist
\item
  Geth (go-ethereum): \url{https://github.com/ethereum/go-ethereum}
\item
  WebThree (cpp-ethereum):
  \url{https://github.com/ethereum/cpp-ethereum}
\item
  Parity: \url{https://github.com/paritytech/parity}
\item
  More: \url{https://www.ethereum.org/cli}
\end{itemize}

These are full client implementations that include mining, networking,
blocks and transaction processing. You should use these clients after
you've sufficiently tested your dapp with Ganache or Truffle Develop and
you're ready to deploy to your desired Ethereum network.

\hypertarget{deploying-to-private-networks}{%
\subsection{Deploying to private
networks}\label{deploying-to-private-networks}}

Private networks utilize the same technology as with live networks, but
with a different configuration. So you can configure any of the Ethereum
clients mentioned above to run a private network, and deploy to it in
exactly the same way.

\end{document}
